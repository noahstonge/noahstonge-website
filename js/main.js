var home = document.getElementById("home-link");
var projects = document.getElementById("projects-link");
var whitepapers = document.getElementById("whitepapers-link");

// Initially give home is-active
// home.classList.add("is-active");

function removeClass(linkOne, linkTwo) {
    $(linkOne).removeClass("is-active")
    $(linkTwo).removeClass("is-active")
}

$(home).on('click', function(){
    home.classList.add("is-active");
    removeClass(projects, whitepapers);
});

$(projects).on('click', function(){
    projects.classList.add("is-active");
    removeClass(home, whitepapers);
});

$(whitepapers).on('click', function(){
    whitepapers.classList.add("is-active");
    removeClass(home, projects);
});
